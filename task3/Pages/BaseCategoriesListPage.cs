﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace task3.Pages
{
    abstract internal class BaseCategoriesListPage : BasePage
    {
        private string CategoriesListXpath => "//div[@class='brand-box__info']";

        public IReadOnlyCollection<IWebElement> GetProductsCategories()
        {
            return FindElements(By.XPath(CategoriesListXpath));
        }

        public void ClickProductCategory(string category)
        {
            FindElements(By.XPath(CategoriesListXpath))
                .Select(x => x.FindElement(By.XPath(".//a[not(@class)]")))
                .Single(x => x.Text.Equals(category))
                .Click();
        }
    }
}
