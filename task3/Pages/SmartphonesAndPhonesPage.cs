﻿namespace task3.Pages
{
    internal class SmartphonesAndPhonesPage : BaseCategoriesListPage
    {
        private static SmartphonesAndPhonesPage smartphonesAndPhonesPage;
        public static SmartphonesAndPhonesPage Instance => smartphonesAndPhonesPage ??= new SmartphonesAndPhonesPage();
    }
}
