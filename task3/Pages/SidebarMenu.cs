﻿using OpenQA.Selenium;
using System.Linq;

namespace task3.Pages
{
    internal class SidebarMenu : BasePage
    {
        private string FirslLevelMenuItemXpath => "//div[contains(@class, 'first-level')]/ul/li";

        private static SidebarMenu sidebarMenu;
        public static SidebarMenu Instance => sidebarMenu ??= new SidebarMenu();

        public void ClickMainMenuItem(string item)
        {
            var menuItems = FindElements(By.XPath(FirslLevelMenuItemXpath));
            var menuItem = menuItems.Single(x => x.FindElement(By.XPath("./a/span/span")).Text.Equals(item));
            menuItem.Click();
        }
    }
}
