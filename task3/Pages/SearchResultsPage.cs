﻿namespace task3.Pages
{
    internal class SearchResultsPage : BaseProductsListPage
    {
        private static SearchResultsPage searchResultsPage;
        public static SearchResultsPage Instance => searchResultsPage ??= new SearchResultsPage();
    }
}
