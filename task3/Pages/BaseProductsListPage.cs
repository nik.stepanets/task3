﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using task3.Base;

namespace task3.Pages
{
    abstract internal class BaseProductsListPage : BasePage
    {
        private string SortOrderDropdownXpath => "//div[@class='two-column-wrapper ']//span[@role='combobox']/span[@id]";
        private string SortOptionXpath => "//li[@role='treeitem']";
        private string ProductsListXpath => "//div[contains(@class, 'item-prod')]";

        private string SeeMoreButtonXpath => "//a[contains(@class, 'btn-see-more')]";

        public string GetSelectedSortOrder()
        {
            return FindElement(By.XPath(SortOrderDropdownXpath)).Text;
        }
        public void SetSorting(string sortOrder)
        {
            FindElement(By.XPath(SortOrderDropdownXpath)).Click();
            FindElements(By.XPath(SortOptionXpath)).Single(x => x.Text.Equals(sortOrder)).Click();
        }
        public List<string> GetProductsDescription()
        {
            var products = FindElements(By.XPath(ProductsListXpath));
            return products.Select(item => item.FindElement(By.XPath(".//div[@class='prod-cart__descr']")).Text).ToList();
        }

        public List<int> GetProductsPrices()
        {
            var productsPrices = FindElements(By.XPath(ProductsListXpath))
                .Select(item => item.FindElement(By.XPath(".//div[@class='prod-cart__prise']")).Text).ToList();

            static int FindPrice(string x)
            {
                Regex regex = new Regex(@"\d+");
                var matches = regex.Matches(x);

                if (matches.Count > 0)
                {
                    return int.Parse(matches[^1].ToString());
                }
                else
                    return 0;
            }
            return productsPrices.Select(x => FindPrice(x)).ToList();
        }

        public int GetProductsCount()
        {
            return FindElements(By.XPath(ProductsListXpath)).Count;
        }

        public void ClickSeeMoreButton(int timeout = 10)
        {
            FindElement(By.XPath(SeeMoreButtonXpath)).Click();

            var wait = new WebDriverWait(DriverManager.Instance(), TimeSpan.FromSeconds(timeout));
            wait.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return jQuery.active == 0"));
        }
    }
}
