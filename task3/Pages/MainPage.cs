﻿using OpenQA.Selenium;

namespace task3.Pages
{
    internal class MainPage : BasePage
    {
        private string SearchField => "//input[@id='input_search']";

        private static MainPage mainPage;
        public static MainPage Instance => mainPage ??= new MainPage();

        public void Search(string query)
        {
            var searchField = FindElement(By.XPath(SearchField));
            searchField.Clear();
            searchField.SendKeys(query);
            searchField.Submit();
        }
    }
}
