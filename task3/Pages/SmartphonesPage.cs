﻿namespace task3.Pages
{
    internal class SmartphonesPage : BaseProductsListPage
    {
        private static SmartphonesPage smartphonesPage;
        public static SmartphonesPage Instance => smartphonesPage ??= new SmartphonesPage();
    }
}
