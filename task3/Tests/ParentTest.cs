﻿using NUnit.Framework;
using task3.Base;

namespace task3.Tests
{
    abstract internal class ParentTest
    {
        [SetUp]
        public void Setup()
        {
            DriverManager.Instance().Navigate().GoToUrl("https://avic.ua");
        }

        [TearDown]
        public void Teardown()
        {
            DriverManager.QuitDriver();
        }
    }
}
