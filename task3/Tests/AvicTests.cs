﻿using NUnit.Framework;
using task3.Pages;

namespace task3.Tests
{
    [TestFixture]
    internal class AvicTests : ParentTest
    {
        [Test]
        public void TestSearchKeyword()
        {
            string keyword = "Nokia";

            MainPage.Instance.Search(keyword);
            var searchResult = SearchResultsPage.Instance.GetProductsDescription();
            foreach (string description in searchResult)
            {
                Assert.That(description, Does.Contain(keyword));
            }
        }

        [Test]
        public void TestSearchKeywords()
        {
            string keyword1 = "Dell";
            string keyword2 = "HP";

            MainPage.Instance.Search($"{keyword1} {keyword2}");
            var searchResult = SearchResultsPage.Instance.GetProductsDescription();
            foreach (string description in searchResult)
            {
                Assert.That(description.Contains(keyword1) || description.Contains(keyword2), Is.True);
            }
        }

        [Test]
        public void TestDefaultSorting()
        {
            SidebarMenu.Instance.ClickMainMenuItem("Смартфони та телефони");
            SmartphonesAndPhonesPage.Instance.ClickProductCategory("Мобільні телефони");
            var selectedOrder = SmartphonesPage.Instance.GetSelectedSortOrder();
            Assert.That(selectedOrder, Is.EqualTo("За популярністю"));
        }

        [Test]
        public void TestSortingIncreasePrice()
        {
            SidebarMenu.Instance.ClickMainMenuItem("Смартфони та телефони");
            SmartphonesAndPhonesPage.Instance.ClickProductCategory("Смартфони");
            SmartphonesPage.Instance.SetSorting("Від дешевих до дорогих");
            Assert.That(SmartphonesPage.Instance.GetProductsPrices(), Is.Ordered.Ascending);
        }

        [Test]
        public void TestProductsQuantityOnPage()
        {
            SidebarMenu.Instance.ClickMainMenuItem("Ноутбуки та планшети");
            LaptopsAndTabletsPage.Instance.ClickProductCategory("Ноутбуки");
            Assert.That(LaptopsPage.Instance.GetProductsCount(), Is.EqualTo(12));
            LaptopsPage.Instance.ClickSeeMoreButton();
            Assert.That(LaptopsPage.Instance.GetProductsCount(), Is.EqualTo(24));
        }
    }
}